//
//  StartUpCollectionViewCell.swift
//  StartUpFest
//
//  Created by Leonardo Bortolotti on 22/05/19.
//  Copyright © 2019 Onovolab. All rights reserved.
//

import UIKit

class StartUpCollectionViewCell: UICollectionViewCell {

    @IBOutlet var image: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var segment: UILabel!
    @IBOutlet var containerView: UIView!
    
}
