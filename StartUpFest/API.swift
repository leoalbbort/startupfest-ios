//  This file was automatically generated and should not be edited.

import Apollo

public final class StartUpsQuery: GraphQLQuery {
  public let operationDefinition =
    "query StartUps {\n  allStartups {\n    __typename\n    name\n    imageUrl\n    description\n    Segment {\n      __typename\n      id\n      code\n      name\n    }\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("allStartups", type: .list(.object(AllStartup.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(allStartups: [AllStartup?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "allStartups": allStartups.flatMap { (value: [AllStartup?]) -> [ResultMap?] in value.map { (value: AllStartup?) -> ResultMap? in value.flatMap { (value: AllStartup) -> ResultMap in value.resultMap } } }])
    }

    public var allStartups: [AllStartup?]? {
      get {
        return (resultMap["allStartups"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AllStartup?] in value.map { (value: ResultMap?) -> AllStartup? in value.flatMap { (value: ResultMap) -> AllStartup in AllStartup(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [AllStartup?]) -> [ResultMap?] in value.map { (value: AllStartup?) -> ResultMap? in value.flatMap { (value: AllStartup) -> ResultMap in value.resultMap } } }, forKey: "allStartups")
      }
    }

    public struct AllStartup: GraphQLSelectionSet {
      public static let possibleTypes = ["Startup"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("imageUrl", type: .nonNull(.scalar(String.self))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("Segment", type: .object(Segment.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String, imageUrl: String, description: String, segment: Segment? = nil) {
        self.init(unsafeResultMap: ["__typename": "Startup", "name": name, "imageUrl": imageUrl, "description": description, "Segment": segment.flatMap { (value: Segment) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var imageUrl: String {
        get {
          return resultMap["imageUrl"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var description: String {
        get {
          return resultMap["description"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }

      public var segment: Segment? {
        get {
          return (resultMap["Segment"] as? ResultMap).flatMap { Segment(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "Segment")
        }
      }

      public struct Segment: GraphQLSelectionSet {
        public static let possibleTypes = ["Segment"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("code", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, code: String, name: String) {
          self.init(unsafeResultMap: ["__typename": "Segment", "id": id, "code": code, "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var code: String {
          get {
            return resultMap["code"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "code")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }
    }
  }
}