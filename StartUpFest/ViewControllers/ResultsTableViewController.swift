//
//  ResultsTableViewController.swift
//  StartUpFest
//
//  Created by Leonardo Bortolotti on 23/05/19.
//  Copyright © 2019 Onovolab. All rights reserved.
//

import UIKit
import Parse
import Kingfisher

class ResultsTableViewController: UITableViewController {
    
    var startupsArray: [StartUpsQuery.Data.AllStartup] = []
    var objectsArray: [[PFObject]] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Resultados"
        
        queryTotalObjects()
    }
    
    
    // MARK: - Helper Methods
    
    func queryTotalObjects() {
        // Inicializa o array e adiciona 3 arrays vazios, um para cada categoria do ranking
        objectsArray = []
        let array = [PFObject]()
        objectsArray.append(array)
        objectsArray.append(array)
        objectsArray.append(array)
        
        // Query dos resultados totais do ranking
        let query = PFQuery(className: Constants.kPARSE_TOTAL_CLASS)
        query.order(byDescending: Constants.kPARSE_AVERAGE_RATING)
        query.findObjectsInBackground { (objects, error) in
            for object in objects! {
                self.objectsArray[object[Constants.kPARSE_CATEGORY] as! Int].append(object)
            }
            self.tableView.reloadData()
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StartUpCell", for: indexPath) as! StartUpTableViewCell

        let startupObject = objectsArray[indexPath.section][indexPath.row]
        for object in startupsArray {
            if object.name == startupObject[Constants.kPARSE_STARTUP] as! String {
                let startup = object
                cell.position.text = String(format: "%dº", indexPath.row + 1)
                cell.name.text = startup.name
                cell.segment.text = startup.segment?.name
                
                cell.logoImageView.kf.setImage(with: URL(string: startup.imageUrl))
                cell.logoImageView.contentMode = .scaleToFill
                cell.logoImageView.layer.cornerRadius = 10
                cell.logoImageView.clipsToBounds = true
                
                let averageRating = startupObject[Constants.kPARSE_AVERAGE_RATING] as! Double
                cell.rating.settings.updateOnTouch = false
                cell.rating.rating = averageRating
                cell.average.text = String(format: "%.1f / 5", averageRating)
                
                break
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! ResultsHeaderTableViewCell
        switch section {
        case Constants.kCATEGORY_PROPOSTA:
            cell.title.text = "Proposta"
        case Constants.kCATEGORY_APRESENTACAO:
            cell.title.text = "Apresentação / Pitch"
        case Constants.kCATEGORY_DESENVOLVIMENTO:
            cell.title.text = "Desenvolvimento"
        default:
            cell.title.text = ""
        }

        // Fonte: https://stackoverflow.com/questions/30953201/adding-blur-effect-to-background-in-swift
        // Adiciona um Blur effect no header
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = cell.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cell.addSubview(blurEffectView)
        cell.sendSubviewToBack(blurEffectView)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
