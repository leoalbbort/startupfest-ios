//
//  ResultsHeaderTableViewCell.swift
//  StartUpFest
//
//  Created by Leonardo Bortolotti on 23/05/19.
//  Copyright © 2019 Onovolab. All rights reserved.
//

import UIKit

class ResultsHeaderTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    
}
