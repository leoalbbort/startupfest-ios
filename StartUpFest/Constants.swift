//
//  Constants.swift
//  StartUpFest
//
//  Created by Leonardo Bortolotti on 23/05/19.
//  Copyright © 2019 Onovolab. All rights reserved.
//

class Constants {
    static let kPARSE_RATING_CLASS = "Rating"
    static let kPARSE_TOTAL_CLASS = "Total"
    static let kPARSE_STARTUP = "startup"
    static let kPARSE_RATING = "rating"
    static let kPARSE_CATEGORY = "category"
    static let kPARSE_INSTALLATION = "installation"
    static let kPARSE_COUNT = "count"
    static let kPARSE_TOTAL_RATING = "totalRating"
    static let kPARSE_AVERAGE_RATING = "averageRating"
    
    // Padrão utilizado para identificar as categorias
    static let kCATEGORY_PROPOSTA = 0
    static let kCATEGORY_APRESENTACAO = 1
    static let kCATEGORY_DESENVOLVIMENTO = 2
}
