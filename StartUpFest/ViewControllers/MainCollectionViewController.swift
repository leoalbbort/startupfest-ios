//
//  MainCollectionViewController.swift
//  StartUpFest
//
//  Created by Leonardo Bortolotti on 22/05/19.
//  Copyright © 2019 Onovolab. All rights reserved.
//

import UIKit
import Apollo
import Kingfisher

private let reuseIdentifier = "StartUpCell"

class MainCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var startupsArray: [StartUpsQuery.Data.AllStartup] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Query das Startups
        apollo.fetch(query: StartUpsQuery()) { (result, error) in
            self.startupsArray = result?.data?.allStartups as! [StartUpsQuery.Data.AllStartup]
            self.collectionView.reloadData()
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return startupsArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StartUpCollectionViewCell
        var startup = startupsArray[indexPath.row]
        
        cell.name.text = startup.name
        cell.segment.text = startup.segment?.name
        
        cell.image.kf.setImage(with: URL(string: startup.imageUrl))
        cell.image.contentMode = .scaleToFill
        cell.image.layer.cornerRadius = 10
        cell.image.clipsToBounds = true
    
        return cell
    }
    
    // Resize na cell para adaptar ao width
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.width - 50)
        }
        else {
            // Quando for iPad, mostrar 2 itens por linha
            return CGSize(width: self.view.bounds.size.width/2 - 10, height: self.view.bounds.size.width/2 - 50)
        }
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MainToDescriptionSegue", sender: startupsArray[indexPath.row])
    }
    
    // Funções para realizar animação de click na cell
    override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! StartUpCollectionViewCell
        cell.containerView.shadowOpacity = 0.3
    }

    override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! StartUpCollectionViewCell
        cell.containerView.shadowOpacity = 1
    }

    
    // MARK: Segue
    
    // Passar a startup selecionada como parâmetro da segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MainToDescriptionSegue" {
            let descriptionViewController = segue.destination as! DescriptionViewController
            descriptionViewController.startupsArray = startupsArray
            descriptionViewController.startup = sender as? StartUpsQuery.Data.AllStartup
        }
        else if segue.identifier == "MainToResultsSegue" {
            let resultsViewController = segue.destination as! ResultsTableViewController
            resultsViewController.startupsArray = startupsArray
        }
        
        // Mudar botão "Voltar" na ViewController seguinte
        let backItem = UIBarButtonItem()
        backItem.title = "Voltar"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
    }

}
