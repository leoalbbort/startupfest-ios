//
//  DescriptionViewController.swift
//  StartUpFest
//
//  Created by Leonardo Bortolotti on 22/05/19.
//  Copyright © 2019 Onovolab. All rights reserved.
//

import UIKit
import Cosmos
import Parse
import Kingfisher

class DescriptionViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var segmentLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var propostaRating: CosmosView!
    @IBOutlet var apresentacaoRating: CosmosView!
    @IBOutlet var desenvolvimentoRating: CosmosView!
    
    var startupsArray: [StartUpsQuery.Data.AllStartup] = []
    var startup: StartUpsQuery.Data.AllStartup!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = startup.name
        
        // Adiciona o botão de ranking programaticamente
        let rankingButton = UIBarButtonItem(image: UIImage(named: "ranking"), style: .plain, target: self, action: #selector(rankingButtonPressed))
        rankingButton.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem  = rankingButton
        
        imageView.kf.setImage(with: URL(string: startup.imageUrl))
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        
        nameLabel.text = startup.name
        segmentLabel.text = startup.segment?.name
        descriptionLabel.text = startup.description
        
        // Configura ações de click nas estrelas de rating
        propostaRating.didFinishTouchingCosmos = { rating in
            print(rating)
            self.saveRating(rating: rating, category: Constants.kCATEGORY_PROPOSTA)
        }
        apresentacaoRating.didFinishTouchingCosmos = { rating in
            print(rating)
            self.saveRating(rating: rating, category: Constants.kCATEGORY_APRESENTACAO)
        }
        desenvolvimentoRating.didFinishTouchingCosmos = { rating in
            print(rating)
            self.saveRating(rating: rating, category: Constants.kCATEGORY_DESENVOLVIMENTO)
        }
    }
    
    // Salva o rating no banco de dados
    func saveRating(rating: Double, category: Int) {
        // Salva um objeto separado para cada rating. Não é utilizado nesse primeiro momento para realizar os cálculos, mas pode ser necessário caso uma nova feature seja adicionada. Serve também como um Log.
        let ratingObject = PFObject(className: Constants.kPARSE_RATING_CLASS)
        ratingObject[Constants.kPARSE_STARTUP] = startup.name
        ratingObject[Constants.kPARSE_RATING] = rating
        ratingObject[Constants.kPARSE_CATEGORY] = category
        ratingObject[Constants.kPARSE_INSTALLATION] = PFInstallation.current()
        ratingObject.saveInBackground()
        
        // Salva e atualiza os ratings totais das startups
        let query = PFQuery(className: Constants.kPARSE_TOTAL_CLASS)
        query.whereKey(Constants.kPARSE_STARTUP, equalTo: startup.name)
        query.whereKey(Constants.kPARSE_CATEGORY, equalTo: category)
        query.findObjectsInBackground { (objects, error) in
            if objects!.count > 0 {
                let totalObject = objects!.first
                var count = totalObject![Constants.kPARSE_COUNT] as! Double
                count += 1
                var totalRating = totalObject![Constants.kPARSE_TOTAL_RATING] as! Double
                totalRating += rating
                totalObject![Constants.kPARSE_COUNT] = count
                totalObject![Constants.kPARSE_TOTAL_RATING] = totalRating
                totalObject![Constants.kPARSE_AVERAGE_RATING] = totalRating / count
                print(totalRating / count)
                totalObject?.saveInBackground()
            }
            else {
                let totalObject = PFObject(className: Constants.kPARSE_TOTAL_CLASS)
                totalObject[Constants.kPARSE_STARTUP] = self.startup.name
                totalObject[Constants.kPARSE_CATEGORY] = category
                totalObject[Constants.kPARSE_COUNT] = 1
                totalObject[Constants.kPARSE_TOTAL_RATING] = rating
                totalObject[Constants.kPARSE_AVERAGE_RATING] = rating
                totalObject.saveInBackground()
            }
        }
    }
    
    
    // MARK: Button Pressed
    
    @objc func rankingButtonPressed() {
        performSegue(withIdentifier: "DescriptionToResultsSegue", sender: nil)
        // Limpa o rating anterior
        propostaRating.rating = 0
        apresentacaoRating.rating = 0
        desenvolvimentoRating.rating = 0
    }
    
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DescriptionToResultsSegue" {
            let resultsViewController = segue.destination as! ResultsTableViewController
            resultsViewController.startupsArray = startupsArray
        }
        
        // Mudar botão "Voltar" na ViewController seguinte
        let backItem = UIBarButtonItem()
        backItem.title = "Voltar"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
    }
}
