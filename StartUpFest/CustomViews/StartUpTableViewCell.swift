//
//  StartUpTableViewCell.swift
//  StartUpFest
//
//  Created by Leonardo Bortolotti on 23/05/19.
//  Copyright © 2019 Onovolab. All rights reserved.
//

import UIKit
import Cosmos

class StartUpTableViewCell: UITableViewCell {

    @IBOutlet var position: UILabel!
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var segment: UILabel!
    @IBOutlet var rating: CosmosView!
    @IBOutlet var average: UILabel!
    
}
